import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import html2canvas from 'html2canvas';

function App() {

  //Variable, function
  const [linea1, setLinea1] = useState('');
  const [linea2, setLinea2] = useState('');
  const [image, setImage] = useState('fire');


  const onChangeLinea1 = (valor) => {
    //alert(valor.target.value)
    setLinea1(valor.target.value);
  }

  const onChangeLinea2 = (valor) => {
    //alert(valor.target.value)
    setLinea2(valor.target.value);
  }

  const onChangeImagen = (valor) => {
    //alert(valor.target.value)
    console.log(valor.target.value);
    setImage(valor.target.value);
  }

  const onClickExport = (valor) => {
    //alert('Export')
    html2canvas(document.querySelector("#meme")).then(canvas => {
      //document.body.appendChild(canvas)
      var img = canvas.toDataURL("image/png")

      var link = document.createElement('a');
      link.download = image;
      link.href = img;
      link.click();
    });
  }

  return (
    <div className="App">

      <select onChange={onChangeImagen}>
        <option value="fire">Casa en llamas</option>
        <option value="futurama">Futurama</option>
        <option value="history">History Channel</option>
        <option value="matrix">Matrix</option>
        <option value="philosoraptor">Philosoraptor</option>
        <option value="smart">Smart Guy</option>
      </select><br />
      <input onChange={onChangeLinea1} type="text" placeholder='Linea 1' /> <br />
      <input onChange={onChangeLinea2} type="text" placeholder='Linea 2' /> <br />
      <button onClick={onClickExport}>Exportar</button>

      <div className='meme' id='meme'>
        <span>{linea1}</span><br />
        <span>{linea2}</span><br />
        <img src={"/img/" + image + ".jpg"} alt="" /><br />
      </div>
      <br />
    </div>
  );
}

export default App;
